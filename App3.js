const http=require('http');
const fs=require('fs')

//file system module

http.createServer((req,res)=>{
    fs.readFile('hello.html',function(err,data) {
        res.writeHead(200,{'Content-Type':'text/html'});
        res.write(data);
        return res.end();
    });
}).listen(8080)

    
fs.writeFile('file.text','welcome Node js',(err)=>{
    if(err) {
        console.log(err);
    }
    console.log('replaced')
})
    
fs.appendFile('file.text','Hello content',(err)=>{
    if(err){
        console.log(err);
    }
    console.log("data added")
})


fs.writeFile('file.text','welcome Node js',(err)=>{
    if(err) {
        console.log(err);
    }
    console.log('replaced')
})

fs.rename('Hello.html','hello.html',(err)=>{
    if(err) {
        console.log(err)
    }
    console.log('file renamed')
})

fs.unlink('file1.text',(err)=>{
    if(err) {
        console.log(err);
    }
    console.log('deleted')
})
